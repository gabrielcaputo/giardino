/*
################################################################################
################################################################################
####                                                                        ####
####                     _____ _           _ _                              ####
####                    |   __|_|___ ___ _| |_|___ ___                      ####
####                    |  |  | | .'|  _| . | |   | . |                     ####
####                    |_____|_|__,|_| |___|_|_|_|___|                     ####
####                                                                        ####
####     Giardino 2017 - Projeto desenvolvido por Gabriel Caputo Mateus     ####
####                                                                        ####
################################################################################
################################################################################
*/



// ##########################################
// ########         Funções          ########
// ##########################################

void InitWiFi() {
  if (wifi.setOprToStation()) {
    Serial.print("Módulo WiFi em modo Station\r\n");
  } else {
    Serial.print("Erro ao configurar o Módulo WiFi em modo Station\r\n");
    tone(25,349,500);
  }

  if (wifi.joinAP(WIFI_SSID, WIFI_PASSWORD)) {
    Serial.print("Conectado na rede WiFi ");
    Serial.print(WIFI_SSID);
    Serial.print("\r\n");
    Serial.println("Conectado!");
    Serial.println("IP: ");
    Serial.println(wifi.getLocalIP().c_str());
  } else {
    Serial.print("Não foi possível conectar na rede WiFi ");
    Serial.print(WIFI_SSID);
    Serial.print("\r\n");
    tone(25,349,500);
  }
  wifi.disableMUX();
}

void CreateIcons() {
  lcd.createChar(0, icon_thermometer);
  lcd.createChar(1, icon_celsius);
  lcd.createChar(2, icon_air);
  lcd.createChar(3, icon_soil);
  lcd.createChar(4, icon_uv0);
  lcd.createChar(5, icon_sun0);
  lcd.createChar(6, icon_bulbOff);
  lcd.createChar(7, icon_drop0);

  // lcd.write((uint8_t)NUMERO);
}

void RemoteConfig() {
  Get("sensors_options?order=id,desc&page=1,1&transform=1");
  DynamicJsonBuffer jsonBuffer;
  JsonObject& root = jsonBuffer.parseObject(temp_var);
  String solo_umidade_limit_json = root["sensors_options"][0]["solo_umidade_limit"];
  String solo_umidade_min_json = root["sensors_options"][0]["solo_umidade_min"];
  String rega_solo_time_json = root["sensors_options"][0]["rega_solo_time"];
  String rega_folhas_time_json = root["sensors_options"][0]["rega_folhas_time"];

  solo_umidade_limit = solo_umidade_limit_json;
  solo_umidade_min = solo_umidade_min_json;
  rega_solo_time = rega_solo_time_json;
  rega_folhas_time = rega_folhas_time_json;
}

void Clock() {
  unsigned long currentMillisClock = millis();
  if (currentMillisClock - previousMillisClock >= intervalClock) {
    previousMillisClock = currentMillisClock;
    tmElements_t tm;
    if (RTC.read(tm)) {

      ano = String(tmYearToCalendar(tm.Year));

      if (tm.Month < 10) {
        mes = "0" + String(tm.Month);
      } else {
        mes = String(tm.Month);
      }

      if (tm.Day < 10) {
        dia = "0" + String(tm.Day);
      } else {
        dia = String(tm.Day);
      }

      if (tm.Hour < 10) {
        hora = "0" + String(tm.Hour);
      } else {
        hora = String(tm.Hour);
      }

      if (tm.Minute < 10) {
        minuto = "0" + String(tm.Minute);
      } else {
        minuto = String(tm.Minute);
      }

      if (tm.Second < 10) {
        segundo = "0" + String(tm.Second);
      } else {
        segundo = String(tm.Second);
      }

      timestamp = ano + "-" + mes + "-" + dia + "T" + hora + ":" + minuto + ":" + segundo;

      lcd.setCursor(0,0);
      lcd.print("Giardino");
      lcd.setCursor(12,0);
      lcd.print(hora);
      lcd.setCursor(14,0);
      lcd.print(':');
      lcd.setCursor(15,0);
      lcd.print(minuto);
      lcd.setCursor(17,0);
      lcd.print(':');
      lcd.setCursor(18,0);
      lcd.print(segundo);
    }
  }
}

void Sensors() {
  unsigned long currentMillisSensors = millis();
  if (currentMillisSensors - previousMillisSensors >= intervalSensors) {
    previousMillisSensors = currentMillisSensors;

    ar_temperatura = dht.readTemperature();
    ar_umidade = dht.readHumidity();
    // ar_pressao = ;
    // solo_temperatura = map(analogRead(solo_temperatura_pin), 0, solo_temperatura_limit.toInt(), 0, 100);
    // if (solo_temperatura > 100) {
    //   solo_temperatura = 100;
    // }
    solo_umidade = map(analogRead(solo_umidade_pin), 0, solo_umidade_limit.toInt(), 0, 100);
    if (solo_umidade > 100) {
      solo_umidade = 100;
    }
    luz_uv = map(analogRead(luz_uv_pin), 0, 1020, 0, 100);
    luz_intensidade = map(analogRead(luz_intensidade_pin), 0, 1020, 0, 100);

    if (lcdView == 0) {
      lcd.setCursor(0,1);
      lcd.print("Temp. ar:           ");
      lcd.setCursor(12,1);
      lcd.print(ar_temperatura);
      lcd.print(" ");
      lcd.print((char)223);
      lcd.print("C");
      lcd.setCursor(0,2);
      lcd.print("Umid. ar:           ");
      lcd.setCursor(12,2);
      lcd.print(ar_umidade);
      lcd.print(" %");
      lcd.setCursor(0,3);
      lcd.print("Pres. ar:           ");
      // lcd.setCursor(12,3);
      // lcd.print("-");
    }
    else if (lcdView == 1) {
      lcd.setCursor(0,1);
      lcd.print("Temp. solo:         ");
      lcd.setCursor(12,1);
      // lcd.print("-");
      // lcd.print(" ");
      // lcd.print((char)223);
      // lcd.print("C");
      lcd.setCursor(0,2);
      lcd.print("Umid. solo:         ");
      lcd.setCursor(12,2);
      lcd.print(solo_umidade);
      lcd.print(" %");
      lcd.setCursor(0,3);
      lcd.print("Alti. solo:         ");
      lcd.setCursor(12,3);
      // lcd.print("-");
      // lcd.print(" m");
    }
    else if (lcdView == 2) {
      lcd.setCursor(0,1);
      lcd.print("Indice UV:          ");
      lcd.setCursor(12,1);
      lcd.print(luz_uv);
      lcd.setCursor(0,2);
      lcd.print("Intens.Luz:         ");
      lcd.setCursor(12,2);
      lcd.print(luz_intensidade);
      lcd.print(" %");
      lcd.setCursor(0,3);
      lcd.print("                    ");
    }
  }
}

void Buttons() {
  botao1_status = digitalRead(botao1);
  botao2_status = digitalRead(botao2);
  botao3_status = digitalRead(botao3);

  if (botao1_status == HIGH) {
    method = "button";
    RegarSolo();
  }
  if (botao2_status == HIGH) {
    method = "button";
    RegarFolhas();
  }
  if (botao3_status == HIGH) {
    // Acender a luz
    InterruptorLuz();
  }
}

void Post(String path, String data) {
  // uint8_t buffer[1024] = {0};
  if (wifi.createTCP(HOST_NAME, HOST_PORT)) {
    Serial.println("TCP iniciado\r\n");
  } else {
    Serial.println("Erro na criação do TCP\r\n");
    return;
  }
  String http = String();
  http += "POST /giardino/api.php/";
  http += path;
  http += " HTTP/1.1\r\n";
  http += "Host: ";
  http += HOST_NAME;
  http += "\r\n";
  http += "Content-Type: application/x-www-form-urlencoded\r\n";
  http += "Cache-Control: no-cache\r\n";
  http += "Content-Length: ";
  http += data.length();
  http += "\r\n\r\n";
  http += data;

  // Serial.println(http);
  wifi.send((const uint8_t*)http.c_str(), http.length());

  RemoteConfig();

  PiscaLed(100);

  // uint32_t len = wifi.recv(buffer, sizeof(buffer), 10000);
  // if (len > 0) {
  //   Serial.print("Retorno:[");
  //   for(uint32_t i = 0; i < len; i++) {
  //     Serial.print((char)buffer[i]);
  //   }
  //   Serial.print("]\r\n");
  // }

}

void Get(String data) {
  uint8_t buffer[1024] = {0};
  if (wifi.createTCP(HOST_NAME, HOST_PORT)) {
    Serial.println("TCP iniciado\r\n");
  } else {
    Serial.println("Erro na criação do TCP\r\n");
    return;
  }

  String http = String();
  http += "GET /giardino/api.php/";
  http += data;
  http += "\r\n";
  http += "Host: ";
  http += HOST_NAME;
  http += "\r\n";
  http += "Cache-Control: no-cache\r\n";
  http += "Connection: close";

  // Serial.println(http);
  wifi.send((const uint8_t*)http.c_str(), http.length());

  uint32_t len = wifi.recv(buffer, sizeof(buffer), 10000);
  if (len > 0) {
    String result = String();
    for(uint32_t i = 0; i < len; i++) {
      // Serial.print((char)buffer[i]);
      result += (char)buffer[i];
    }

    // split = result.lastIndexOf("__SPLIT__");
    // result = result.substring(split + 9, len);

    temp_var = result;
  }

  PiscaLed(100);
}

void Put(String path, String data) {
  // uint8_t buffer[1024] = {0};
  if (wifi.createTCP(HOST_NAME, HOST_PORT)) {
    Serial.println("TCP iniciado\r\n");
  } else {
    Serial.println("Erro na criação do TCP\r\n");
    return;
  }
  String http = String();
  http += "PUT /giardino/api.php/";
  http += path;
  http += " HTTP/1.1\r\n";
  http += "Host: ";
  http += HOST_NAME;
  http += "\r\n";
  http += "Content-Type: application/x-www-form-urlencoded\r\n";
  http += "Cache-Control: no-cache\r\n";
  http += "Content-Length: ";
  http += data.length();
  http += "\r\n\r\n";
  http += data;

  // Serial.println(http);
  wifi.send((const uint8_t*)http.c_str(), http.length());

  PiscaLed(100);

  // uint32_t len = wifi.recv(buffer, sizeof(buffer), 10000);
  // if (len > 0) {
  //   Serial.print("Retorno:[");
  //   for(uint32_t i = 0; i < len; i++) {
  //     Serial.print((char)buffer[i]);
  //   }
  //   Serial.print("]\r\n");
  // }

}

void Log() {
  unsigned long currentMillisFirebase = millis();
  if (currentMillisFirebase - previousMillisFirebase >= intervalFirebase) {
    previousMillisFirebase = currentMillisFirebase;

    Serial.println("Gravando informações no banco de dados");
    ClearLCD(0,3);
    lcdView = 30;
    lcd.setCursor(0,1);
    lcd.print("Gravando informacoes");
    lcd.setCursor(0,2);
    lcd.print("no banco de dados");

    // GetFirebase("Trigger");
    // if (temp_var != "0") {
    //   if (temp_var == "botao3") {
    //     SendFirebase("Trigger", "0");
    //     InterruptorLuz();
    //   }
    // }

    String data = String();
    data += "ar_temperatura=";
    data += String(ar_temperatura);
    data += "&ar_umidade=";
    data += String(ar_umidade);
    data += "&ar_pressao=";
    data += String(ar_pressao);
    data += "&solo_temperatura=";
    data += String(solo_temperatura);
    data += "&solo_umidade=";
    data += String(solo_umidade);
    data += "&luz_uv=";
    data += String(luz_uv);
    data += "&luz_intensidade=";
    data += String(luz_intensidade);
    Post("sensors_log", data);

    ClearLCD(0,3);
    lcdView = 0;

  }
}

void LCDPage() {
  unsigned long currentMillisLCDPage = millis();
  if (currentMillisLCDPage - previousMillisLCDPage >= intervalLCDPage) {
    previousMillisLCDPage = currentMillisLCDPage;
    if (lcdView == 0) {
      lcdView = 1;
    } else if (lcdView == 1) {
      lcdView = 2;
    } else if (lcdView == 2) {
      lcdView = 0;
    } else if (lcdView == 10) { // Regando o solo
      ClearLCD(1,3);
      lcd.setCursor(0,2);
      lcd.print("Regando o solo...");
    } else if (lcdView == 20) { // Regando as folhas
      ClearLCD(1,3);
      lcd.setCursor(0,2);
      lcd.print("Regando as folhas...");
    }
  }
}

void RegarSolo() {
  lcdView = 20;
  ClearLCD(0,3);
  lcd.setCursor(0,1);
  lcd.print("Regando o solo...");
  digitalWrite(relay1, HIGH);

  String data = String();
  data += "method=";
  data += method;
  Post("rega_solo_log", data);

  delay(rega_solo_time.toInt());
  ClearLCD(0,3);
  digitalWrite(relay1, LOW);
  lcdView = 0;
}

void RegarFolhas() {
  lcdView = 20;
  ClearLCD(0,3);
  lcd.setCursor(0,1);
  lcd.print("Regando as folhas...");
  digitalWrite(relay2, HIGH);

  String data = String();
  data += "method=";
  data += method;
  Post("rega_folhas_log", data);

  delay(rega_folhas_time.toInt());
  ClearLCD(0,3);
  digitalWrite(relay2, LOW);
  lcdView = 0;
}

void InterruptorLuz() {
  if (lampada == 0) {
    digitalWrite(relay3, HIGH);
    lampada = 1;
  } else {
    digitalWrite(relay3, LOW);
    lampada = 0;
  }
  delay(300);
}

void ClearLCD(int lInicial, int lFinal) {
  int lTemp = lInicial;
  while(lTemp <= lFinal) {
    lcd.setCursor(0,lTemp);
    lcd.print("                    ");
    lTemp++;
  }
}

void PiscaLed(int interval) {
  digitalWrite(ledVerde1, HIGH);
  delay(interval);
  digitalWrite(ledVerde1, LOW);
}
