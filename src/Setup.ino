/*
################################################################################
################################################################################
####                                                                        ####
####                     _____ _           _ _                              ####
####                    |   __|_|___ ___ _| |_|___ ___                      ####
####                    |  |  | | .'|  _| . | |   | . |                     ####
####                    |_____|_|__,|_| |___|_|_|_|___|                     ####
####                                                                        ####
####     Giardino 2017 - Projeto desenvolvido por Gabriel Caputo Mateus     ####
####                                                                        ####
################################################################################
################################################################################
*/



// ##########################################
// ########       Bibliotecas        ########
// ##########################################
#include "OneWire.h"
#include "Wire.h"
#include "TimeLib.h"
#include "DS1307RTC.h"
#include "DHT.h"
#include "LiquidCrystal_I2C.h"
#include "ESP8266.h"
#include "ArduinoJson.h"

using namespace ArduinoJson::Internals;


// ##########################################
// ########          Pinos           ########
// ##########################################

// DHT22: Umidade e temperatura do ar
#define DHTPIN 18
#define DHTTYPE DHT22
DHT dht(DHTPIN, DHTTYPE);

// Umidade do solo
int solo_umidade_pin = 6;

// Índice UV da Luz
int luz_uv_pin = 7;

//Índice de Luminancia
int luz_intensidade_pin = 8;

// Buzzer
int buzzer = 25;

// Led Verde 1
int ledVerde1 = 37;

// Botão 1
int botao1 = 26;
int botao1_status = 0;

// Botão 2
int botao2 = 27;
int botao2_status = 0;

// Botão 3
int botao3 = 28;
int botao3_status = 0;
int lampada = 0;

// Relay 1
int relay1 = 46;

// Relay 2
int relay2 = 47;

// Relay 3
int relay3 = 48;

// LCD
LiquidCrystal_I2C lcd(0x27,2,1,0,4,5,6,7,3, POSITIVE);

byte icon_thermometer[] = {0b00100,0b01010,0b01010,0b01010,0b11111,0b11111,0b01110,0b00000};
byte icon_heart[] = {0b00000,0b00000,0b01010,0b11111,0b01110,0b00100,0b00000,0b00000};
byte icon_celsius[] = {0b11100,0b10100,0b11100,0b00000,0b00111,0b00100,0b00111,0b00000};
byte icon_watch[] = {0b00000,0b01110,0b10101,0b10111,0b10001,0b01110,0b00000,0b00000};
byte icon_air[] = {0b11111,0b11011,0b10101,0b10101,0b10001,0b10101,0b10101,0b11111};
byte icon_soil[] = {0b11111,0b11001,0b10111,0b10111,0b11011,0b11101,0b10011,0b11111};
byte icon_flowerpot[] = {0b00100,0b01110,0b11011,0b01110,0b00100,0b11111,0b11111,0b01110};
byte icon_leaf[] = {0b00100,0b01010,0b10001,0b10101,0b10101,0b11111,0b00100,0b00000};
byte icon_uvtxt[] = {0b01010,0b01010,0b01110,0b00000,0b01010,0b01010,0b00100,0b00000};
byte icon_uv0[] = {0b01110,0b01010,0b01010,0b01010,0b01110,0b00000,0b11111,0b00000};
byte icon_uv1[] = {0b00100,0b00100,0b00100,0b00100,0b00100,0b00000,0b11111,0b00000};
byte icon_uv2[] = {0b01110,0b00010,0b01110,0b01000,0b01110,0b00000,0b11111,0b00000};
byte icon_uv3[] = {0b01110,0b00010,0b01110,0b00010,0b01110,0b00000,0b11111,0b00000};
byte icon_uv4[] = {0b01010,0b01010,0b01110,0b00010,0b00010,0b00000,0b11111,0b00000};
byte icon_uv5[] = {0b01110,0b01000,0b01110,0b00010,0b01110,0b00000,0b11111,0b00000};
byte icon_uv6[] = {0b01110,0b01000,0b01110,0b01010,0b01110,0b00000,0b11111,0b00000};
byte icon_uv7[] = {0b01110,0b00010,0b00100,0b00100,0b00100,0b00000,0b11111,0b00000};
byte icon_uv8[] = {0b01110,0b01010,0b01110,0b01010,0b01110,0b00000,0b11111,0b00000};
byte icon_uv9[] = {0b01110,0b01010,0b01110,0b00010,0b01110,0b00000,0b11111,0b00000};
byte icon_uv10[] = {0b10111,0b10101,0b10101,0b10101,0b10111,0b00000,0b11111,0b00000};
byte icon_uv11[] = {0b01010,0b01010,0b01010,0b01010,0b01010,0b00000,0b11111,0b00000};
byte icon_sun0[] = {0b10101,0b01110,0b11011,0b01110,0b10101,0b00000,0b10000,0b00000};
byte icon_sun25[] = {0b10101,0b01110,0b11011,0b01110,0b10101,0b00000,0b11000,0b00000};
byte icon_sun50[] = {0b10101,0b01110,0b11011,0b01110,0b10101,0b00000,0b11100,0b00000};
byte icon_sun75[] = {0b10101,0b01110,0b11011,0b01110,0b10101,0b00000,0b11110,0b00000};
byte icon_sun100[] = {0b10101,0b01110,0b11011,0b01110,0b10101,0b00000,0b11111,0b00000};
byte icon_bulbOff[] = {0b01110,0b10001,0b10001,0b10001,0b01110,0b01010,0b00100,0b00000};
byte icon_bulbOn[] = {0b01110,0b10001,0b10101,0b10001,0b01110,0b01010,0b00100,0b00000};
byte icon_drop0[] = {0b00000,0b00100,0b01010,0b10001,0b10001,0b10001,0b01110,0b00000};
byte icon_drop25[] = {0b00000,0b00100,0b01010,0b10001,0b10001,0b11111,0b01110,0b00000};
byte icon_drop50[] = {0b00000,0b00100,0b01010,0b10001,0b11111,0b11111,0b01110,0b00000};
byte icon_drop75[] = {0b00000,0b00100,0b01010,0b11111,0b11111,0b11111,0b01110,0b00000};
byte icon_drop100[] = {0b00000,0b00100,0b01110,0b11111,0b11111,0b11111,0b01110,0b00000};


// ##########################################
// ########       Intervalos         ########
// ##########################################

// Relógio: 1 segundo
unsigned long previousMillisClock = 0;
unsigned long intervalClock = 1000;

// Leitura de sensores: 2 segundos
unsigned long previousMillisSensors = 0;
unsigned long intervalSensors = 2000;

// Envia para o Firebase: 30 segundos
unsigned long previousMillisFirebase = 0;
unsigned long intervalFirebase = 30000;

// Troca de página do LCD: 6 segundos
unsigned long previousMillisLCDPage = 0;
unsigned long intervalLCDPage = 6000;


// ##########################################
// ######## Definições e declarações ########
// ##########################################
String ano, mes, dia, hora, minuto, segundo, timestamp, solo_umidade_limit, solo_umidade_min, temp_var, method, rega_solo_time, rega_folhas_time;
float ar_umidade, ar_temperatura, solo_temperatura;
int lcdView, solo_umidade, luz_uv, luz_intensidade, solo_altitude, ar_pressao;


// Define o módulo Wifi e o SSID e senha da rede
ESP8266 wifi(Serial3, 115200);
// #define WIFI_SSID        "KOMBI 47"
#define WIFI_SSID        "fullbar-prod"
// #define WIFI_PASSWORD    "985274579"
#define WIFI_PASSWORD    "aa11bb22cc"

// Define o host do FirebasePHP
#define HOST_NAME "gabrielcaputo.com.br"
#define HOST_PORT (80)


// ##########################################
// ########          Setup           ########
// ##########################################
void setup()
{

  // Inicialização
  pinMode(buzzer,OUTPUT);
  pinMode(ledVerde1, OUTPUT);
  pinMode(botao1, INPUT);
  pinMode(botao2, INPUT);
  pinMode(botao3, INPUT);
  pinMode(relay1, OUTPUT);
  pinMode(relay2, OUTPUT);
  pinMode(relay3, OUTPUT);

  Wire.begin();
  Serial.begin(9600);

  lcd.begin (20,4);

  CreateIcons();

  PiscaLed(50);

  lcd.setBacklight(HIGH);
  ClearLCD(0,3);
  lcd.setCursor(0,1);
  tone(25,1500,100);
  lcd.print("Inicializando...");
  Serial.print("Inicializando...\r\n");
  PiscaLed(50);
  dht.begin();
  PiscaLed(50);
  InitWiFi();
  PiscaLed(50);
  RemoteConfig();
  PiscaLed(50);

  // Inicializado
  ClearLCD(0,3);
  PiscaLed(1000);

}
