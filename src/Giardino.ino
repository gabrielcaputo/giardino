/*
################################################################################
################################################################################
####                                                                        ####
####                     _____ _           _ _                              ####
####                    |   __|_|___ ___ _| |_|___ ___                      ####
####                    |  |  | | .'|  _| . | |   | . |                     ####
####                    |_____|_|__,|_| |___|_|_|_|___|                     ####
####                                                                        ####
####     Giardino 2017 - Projeto desenvolvido por Gabriel Caputo Mateus     ####
####                                                                        ####
################################################################################
################################################################################
*/



void loop() {
  Buttons();
  Clock();
  Sensors();
  Log();
  LCDPage();
}
